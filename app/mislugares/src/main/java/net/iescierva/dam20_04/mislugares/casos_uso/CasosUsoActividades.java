package net.iescierva.dam20_04.mislugares.casos_uso;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Toast;


import net.iescierva.dam20_04.mislugares.presentacion.AcercaDeActivity;
import net.iescierva.dam20_04.mislugares.presentacion.EdicionLugarActivity;
import net.iescierva.dam20_04.mislugares.presentacion.PreferenciasActivity;
import net.iescierva.dam20_04.mislugares.presentacion.PrimosActivity;

public class CasosUsoActividades {

    private Activity actividad;

    public CasosUsoActividades(Activity actividad){
        this.actividad = actividad;
    }

    public void lanzarAcercaDe(View view) {
        Intent i = new Intent(actividad, AcercaDeActivity.class);
        actividad.startActivity(i);
    }

    public void mostrarPreferencias(View view){
        SharedPreferences pref =
                PreferenceManager.getDefaultSharedPreferences(actividad);
        String s = "notificaciones: "+ pref.getBoolean("notificaciones",true)
                +", e-mail: "+pref.getString("email","")
                +", tiposNotificaciones: "+pref.getString("tiposNotificaciones", "0")
                +", máximo a mostrar: " + pref.getString("maximo","?")
                +", orden: "+pref.getString("orden","")
                ;
        Toast.makeText(actividad, s, Toast.LENGTH_SHORT).show();
    }

    public void lanzarPreferencias(View view){
        Intent i = new Intent(actividad, PreferenciasActivity.class);
        actividad.startActivity(i);
    }

    public void lanzarEditarLugares(View view){
        Intent i = new Intent(actividad, EdicionLugarActivity.class);
        actividad.startActivity(i);
    }

    public void lanzarPrimos(View view){
        Intent i = new Intent(actividad, PrimosActivity.class);
        actividad.startActivity(i);
    }


}