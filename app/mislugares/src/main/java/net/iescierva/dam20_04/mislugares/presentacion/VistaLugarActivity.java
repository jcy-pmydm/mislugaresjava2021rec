package net.iescierva.dam20_04.mislugares.presentacion;

import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

import net.iescierva.dam20_04.mislugares.R;
import net.iescierva.dam20_04.mislugares.casos_uso.CasosUsoLugar;
import net.iescierva.dam20_04.mislugares.datos.RepositorioLugares;
import net.iescierva.dam20_04.mislugares.modelo.Lugar;

public class VistaLugarActivity extends Activity {

    final static int RESULTADO_EDITAR = 1;
    private RepositorioLugares lugares;
    private CasosUsoLugar usoLugar;
    private EdicionLugarActivity editarLugar;
    private int pos;
    private Lugar lugar;



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.vista_lugar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.accion_compartir:
                return true;
            case R.id.accion_llegar:
                return true;
            case R.id.accion_editar:
                usoLugar.editar(pos, RESULTADO_EDITAR);
                return true;
            case R.id.accion_borrar:
                usoLugar.dialogoBorrar(pos);
                return true;
            case R.id.accion_guardar:


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULTADO_EDITAR) {
            editarLugar.actualizaVistas();
            findViewById(R.id.scrollView1).invalidate();
        }
    }

}
