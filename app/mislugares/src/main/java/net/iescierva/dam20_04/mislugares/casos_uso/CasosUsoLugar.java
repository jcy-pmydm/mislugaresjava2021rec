package net.iescierva.dam20_04.mislugares.casos_uso;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import net.iescierva.dam20_04.mislugares.datos.RepositorioLugares;
import net.iescierva.dam20_04.mislugares.modelo.Lugar;
import net.iescierva.dam20_04.mislugares.presentacion.EdicionLugarActivity;
import net.iescierva.dam20_04.mislugares.presentacion.VistaLugarActivity;

public class CasosUsoLugar {
    private Activity actividad;
    private RepositorioLugares lugares;

    public CasosUsoLugar(Activity actividad, RepositorioLugares lugares) {
        this.actividad = actividad;
        this.lugares = lugares;
    }
    // OPERACIONES BÁSICAS
    public void mostrar(int pos) {
        Intent i = new Intent(actividad, VistaLugarActivity.class);
        i.putExtra("pos", pos);
        actividad.startActivity(i);
    }

    public void dialogoBorrar(final int id) {
        new AlertDialog.Builder(actividad)
                .setTitle("Borrado de lugar")
                .setMessage("¿Estás seguro de que quieres eliminar este lugar?")
                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        borrar(id);
                        actividad.finish();
                    }})
                .setNegativeButton("Cancelar", null)
                .show();
    }

    public void borrar(final int id) {
        lugares.delete(id);
        actividad.finish();
    }

    public void guardar(int id, Lugar nuevoLugar) {
        lugares.update_element(id, nuevoLugar);
    }

    public void editar(int pos, int codigoSolicitud) {
        Intent i = new Intent(actividad, EdicionLugarActivity.class);
        i.putExtra("pos", pos);
        actividad.startActivityForResult(i, codigoSolicitud);
    }


}