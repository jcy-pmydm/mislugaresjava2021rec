package net.iescierva.dam20_04.mislugares;

import android.app.Application;

import net.iescierva.dam20_04.mislugares.datos.LugaresLista;
import net.iescierva.dam20_04.mislugares.datos.RepositorioLugares;

public class Aplicacion extends Application {

    private RepositorioLugares lugares = new LugaresLista();
    @Override public void onCreate() {
        super.onCreate();
    }

    public RepositorioLugares getLugares() {
        return lugares;
    }


}