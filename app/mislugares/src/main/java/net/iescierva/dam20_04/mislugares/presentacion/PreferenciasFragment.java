package net.iescierva.dam20_04.mislugares.presentacion;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import net.iescierva.dam20_04.mislugares.R;

public class PreferenciasFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferencias);
    }
}
