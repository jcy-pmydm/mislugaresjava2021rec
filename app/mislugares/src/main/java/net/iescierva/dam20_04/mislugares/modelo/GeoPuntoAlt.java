package net.iescierva.dam20_04.mislugares.modelo;

public class GeoPuntoAlt extends GeoPunto{
    double altura;
    double latitud;
    double longitud;
    double distanciaNueva;
    GeoPunto geoPunto;

    public GeoPuntoAlt( double latitud, double longitud, double altura){
        super(latitud,longitud);
        this.altura = altura;
    }

    public GeoPuntoAlt(GeoPunto p, double altura){
        super(p.getLatitud(), p.getLongitud());
        this.altura = altura;
    }


    public double distancia(GeoPuntoAlt punto) {
        double distanciaOriginal = Math.pow(geoPunto.distancia(punto),2);
        double diferenciaAlturas = Math.pow(altura - punto.altura,2);
        return Math.sqrt(distanciaOriginal+diferenciaAlturas);



        }
    //diferencia_alturas = alturaActual - alturaAnterior;


    public void comprobarAltura(){
        if(altura<-2000 || altura>10000){

        }
    }



}
