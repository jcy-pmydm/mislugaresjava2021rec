package net.iescierva.dam20_04.mislugares.datos;

import net.iescierva.dam20_04.mislugares.modelo.Lugar;

public interface RepositorioLugares {

    void delete(int id);

    Lugar get_element(int pos);

    void update_element(int id, Lugar nuevoLugar);
}