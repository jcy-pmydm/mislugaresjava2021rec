package net.iescierva.dam20_04.mislugares.presentacion;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import net.iescierva.dam20_04.mislugares.Aplicacion;
import net.iescierva.dam20_04.mislugares.R;
import net.iescierva.dam20_04.mislugares.casos_uso.CasosUsoActividades;
import net.iescierva.dam20_04.mislugares.casos_uso.CasosUsoLugar;
import net.iescierva.dam20_04.mislugares.datos.RepositorioLugares;

public class MainActivity extends AppCompatActivity {


    private Button bMostrarLugares;
    private Button bPreferencias;
    private Button bAcercaDe;
    private Button bEditarLugares;
    private Button bSalir;
    private Button bPrimos;
    private RepositorioLugares lugares;
    private CasosUsoLugar usoLugar;
    private CasosUsoActividades casoActividades;
    private MenuItem MPrimos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bMostrarLugares = findViewById(R.id.mostrarLugares);
        bMostrarLugares.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        bPreferencias = findViewById(R.id.Preferencias);
        bPreferencias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                casoActividades.lanzarPreferencias(null);
            }
        });

        bAcercaDe = findViewById(R.id.acercaDe  );
        bAcercaDe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                casoActividades.lanzarAcercaDe(null);
            }
        });

        bEditarLugares = findViewById(R.id.EdicionLugar);
        bEditarLugares.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                casoActividades.lanzarEditarLugares(null);}
        });

        bSalir = findViewById(R.id.Salir);
        bSalir.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });

        bPrimos = findViewById(R.id.Primos);
        bPrimos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                casoActividades.lanzarPrimos(null);
            }
        });
/*
        MPrimos = findViewById(R.id.primosMenu);
        MPrimos.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                casoActividades.lanzarPrimos(null);
                return false;
            }
        });
*/

        casoActividades = new CasosUsoActividades(this);
        lugares =((Aplicacion)this.getApplication()).getLugares();
        usoLugar = new CasosUsoLugar(this, lugares);



    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_menu, menu);
        return true; /** true -> el menú ya está visible */
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            casoActividades.lanzarPreferencias(null);
            return true;
        } else
        if (id == R.id.acercaDe) {
            casoActividades.lanzarAcercaDe(null);
            return true;
        } else
        if (id == R.id.menu_buscar) {
            lanzarVistaLugar(null);
            return true;
        }
        if(id == R.id.primosMenu){
            casoActividades.lanzarPrimos(null);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void lanzarVistaLugar(View view){
        final EditText entrada = new EditText(this);
        entrada.setText("0");
        new AlertDialog.Builder(this)
                .setTitle("Selección de lugar")
                .setMessage("indica su id:")
                .setView(entrada)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        int id = Integer.parseInt (entrada.getText().toString());
                        usoLugar.mostrar(id);
                    }})
                .setNegativeButton("Cancelar", null)
                .show();

    }


}