package net.iescierva.dam20_04.lib2;

public class Principal {
    public static void main(String[] args) {
        Lugar lugar = new Lugar(
                "Escuela Politécnica Superior de Gandía",
                "C/ Paranimf, 1 46730 Gandia (SPAIN)",
                new GeoPunto(38.995656, -0.166093),
                "",
                TipoLugar.OTROS,
                962849300,
                "http://www.epsg.upv.es",
                "Uno de los mejores lugares para formarse.",
                3);
        System.out.println("Lugar " + lugar.toString());
    }
}


